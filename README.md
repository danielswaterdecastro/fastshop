# Fastshop
Este projeto foi gerado com o [Angular CLI](https://github.com/angular/angular-cli) versão 7.2.1.

## Instalação
Fazer o clone do projeto e depois executar o comando `npm install`

## Development server

Execute o comando `ng serve` para rodar o servidor. Navegue no endereço `http://localhost:4200/`. 

## Módulos Externos

Foram umtilizados alguns módulos externos como ngx-bootstrap [https://valor-software.com/ngx-bootstrap/], ng-circle-progress [https://www.npmjs.com/package/ng-circle-progress] flag-icon-css [https://github.com/lipis/flag-icon-css] e Font Awesome [https://fontawesome.com/icons]

# ngx-bootstrap

Módulo componentizado do próprio Bootstrap

# ng-circle-progress

Componente utilizado para se fazer os gráficos de uma forma mais próxima do gráfico original do The Movie DB

# flag-icon-css

Arquivo CSS + SVG para importar as bandeiras

# Font Awesome

SVG para a criação de ícones

## Produção

Para acessar o site em produção, navegar em: `https://danielswater.github.io/fastshop`


